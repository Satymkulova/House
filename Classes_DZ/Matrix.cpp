#include "stdafx.h"
#include "Matrix.h"
#include <iostream>

using namespace std;

void Matrix::vyvod_matr(Matrix Matr1)
{
	cout << endl;
	cout << endl;
	int l1 = Matr1.l1;
	int l2 = Matr1.l2;
	for (int i = 0; i < l1; i++) {
		for (int j = 0; j < l2; j++)
		{
			cout << Matr1.dynamic_matrix[i][j] << " \t";
		}
		cout << std::endl;
	}
	cout << std::endl;
}

Matrix Matrix::matr_slozhenie(Matrix Matr1, Matrix Matr2)
{
	int l3 = Matr1.l1;
	int l5 = Matr2.l1;
	int l4 = Matr1.l2;
	int l6 = Matr2.l2;

	if ((l3 == l5) && (l4 == l6))
	{
		Matrix Otv(Matr1.l1, Matr1.l2);
		int l1 = l3;
		int l2 = l4;
		for (int i = 0; i < l1; i++)
		{
			for (int j = 0; j < l2; j++)
			{
				Otv.dynamic_matrix[i][j] = 0;

				Otv.dynamic_matrix[i][j] = Matr1.dynamic_matrix[i][j] + Matr2.dynamic_matrix[i][j];

			}
		}

		return Otv;
	}

}

Matrix Matrix::raznost_matr(Matrix Matr1, Matrix Matr2)
{
	int l3 = Matr1.l1;
	int l5 = Matr2.l1;
	int l4 = Matr1.l2;
	int l6 = Matr2.l2;

	if ((l3 == l5) && (l4 == l6))
	{
		Matrix Otv(Matr1.l1, Matr1.l2);
		int l1 = l3;
		int l2 = l4;
		for (int i = 0; i < l1; i++)
		{
			for (int j = 0; j < l2; j++)
			{
				Otv.dynamic_matrix[i][j] = 0;

				Otv.dynamic_matrix[i][j] = Matr1.dynamic_matrix[i][j] - Matr2.dynamic_matrix[i][j];

			}
		}

		return Otv;
	}

}


Matrix Matrix::matr_umnozhenie(Matrix Matr1, Matrix Matr2)
{
	int l3 = Matr1.l1;

	int l4 = Matr1.l2;

	int l5 = Matr2.l1;

	int l6 = Matr2.l2;

	if (l3 == l6)
	{
		Matrix Otv(Matr1.l1, Matr2.l2);

		for (int i = 0; i < l3; i++)
		{
			for (int j = 0; j < l6; j++)
			{
				Otv.dynamic_matrix[i][j] = 0;

				for (int k = 0; k < l4; k++)
				{
					Otv.dynamic_matrix[i][j] += (Matr1.dynamic_matrix[i][k] * Matr2.dynamic_matrix[k][j]);
				}

			}
		}
		return Otv;
	}
}

Matrix::Matrix()
{

	cout << "enter matrix length ";
	cin >> l1;
	ll1 = l1;
	cout << "enter matrix height ";
	cin >> l2;
	ll2 = l2;
	dynamic_matrix = new int *[l1];
	for (int i = 0; i < l1; i++)
	{
		dynamic_matrix[i] = new int[l2];

	}

	for (int i = 0; i < l1; i++) {
		for (int j = 0; j < l2; j++)
		{
			dynamic_matrix[i][j] = 0;
		}
	}
}

Matrix::Matrix(int ll1, int ll2)
{
	l1 = ll1;
	l2 = ll2;
	ll1 = l1;
	ll2 = l2;
	dynamic_matrix = new int *[l1];
	for (int i = 0; i < l1; i++)
	{
		dynamic_matrix[i] = new int[l2];

	}

	for (int i = 0; i < l1; i++) {
		for (int j = 0; j < l2; j++)
		{
			dynamic_matrix[i][j] = 0;
		}
	}
}
Matrix::Matrix(int **matrix1Dyn, int l3, int l4)
{
	l1 = l3;
	l2 = l4;
	ll2 = l2;
	ll1 = l3;
	dynamic_matrix = new int *[l1];
	for (int i = 0; i <l3; i++)
	{
		dynamic_matrix[i] = new int[l4];

	}

	for (int i = 0; i<l3; i++)
	{
		for (int j = 0; j < l4; j++) {
			dynamic_matrix[i][j] = matrix1Dyn[i][j];
		}
	}
}


Matrix::~Matrix()
{
	cout << "Detruct";
}
