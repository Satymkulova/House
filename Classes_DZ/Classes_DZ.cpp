// MatrixVect_Classes.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Matrix.h"
#include "Vector.h"


Matrix operator + (Matrix lhs,
	Matrix rhs)
{
	Matrix result(1, 1);
	return result.matr_slozhenie(lhs, rhs);
}

Matrix operator - (Matrix lhs,
	Matrix rhs)
{
	Matrix result(1, 1);
	return result.raznost_matr(lhs, rhs);
}

Matrix operator * (Matrix lhs,
	Matrix rhs)
{
	Matrix result(1, 1);
	return result.matr_umnozhenie(lhs, rhs);
}

ostream & operator << (ostream & os,
	Matrix & rhs)
{
	rhs.vyvod_matr(rhs);
	return os;
}

using namespace std;


int main()
{

	Matrix Matr1;
	Matrix Matr2;

	for (int i = 0; i < Matr1.ll1; i++)
	{
		for (int j = 0; j < Matr1.ll2; j++)
		{
			Matr1.dynamic_matrix[i][j] = 2;
		}
	}

	for (int i = 0; i < Matr2.ll1; i++)
	{
		for (int j = 0; j < Matr2.ll2; j++)
		{
			Matr2.dynamic_matrix[i][j] = 3;
		}
	}

	cout << (Matr1 * Matr2);
	cout << Matr1 + Matr2;
	cout << Matr1 - Matr2;

	system("pause");

	getchar();
	return 0;
}

