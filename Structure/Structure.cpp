// Structure.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <array>
#include<vector>
#include <list>
#include <map>
#include <iostream>
#include <string>
#include <cmath>
#include <exception>

// STD - стандартная библиотека языка С++
// Функции print_f, getchar, fopen, read - из данной библиотеки
// является связующим звеном
// между языком о ОС: [C++] -> [STD] -> [OC]


// STL - standart template library (template - шаблон)
// Это расширение STD, содержащее структуры данных
// и функции по работе с ними.
// Именно из STL подключаются:
// потоки cout, cin, fstream, ifstream, ofstream,
// контейнеры array, vector, list, map,
// итераторы, 
// алгоритмы sort, copy и т.д.


 // std::array
// инкапсулирует массивы неизменной длины, является шаблоном

 // std::vector

 // std:: list

 // std::map

int main()
{
	std::array <double, 10> double_array1; // объект, построенный из шаблона std::array<>, инкапсулирующий массив
	double_array1[5] = 10.5;
	double_array1.fill(0.0);
	double_array1.size();
	int array_length = double_array1.size();
	// double_array.swap(); - функция для обмена между сожержимым двух массивов
	std::sort(double_array1.begin(), double_array1.end());
	std::array <double, 10> double_array2 = { 0 };
	std::copy(double_array1.begin(), double_array1.end(), double_array2.begin());
	// 
	std::array<double, 10>::iterator array_iterator;

	// для создания двумерных массивов
	std::array<std::array<int, 3>, 3> mat33 = {0};
	mat33[1][1] = 100;

	// Итератор - это объект, ссылающийся на элемент массива, вектора или списка
	// "улучшенный" вариант переменной-счётчика
	// Нужен для связи шаблонов STL



	// std::vector
	// Инкапсулирует одномерный массив и предоставляет средства для изменения его длины
	// Очень близким к std::vector по функционалу является std::string
	// ОСНОВНОЙ НЕДОСТАТОК
	// Однако каждый раз при увеличении длины vector запрашивает у ОС
	// новую область памяти и копирует туда старую (старую освобождает)
	// ОСНОВНОЕ ПРЕИМУЩЕСТВО
	// Доступ к элементам осуществляется быстро напрямую по адресу
	// [адрес_1 + i]
	// ВЫВОД
	// доступ быстрый, 

	std::vector<long long> long_vector = {50, 10, 20};

	std::cout << "vector<> = ";
	for (long long value : long_vector)
		std::cout << '\t' << std::endl;
	

	long_vector.push_back(25);
	long_vector.push_back(0);


	std::cout << "vector<> after push() ";
	for (long long value : long_vector)
		std::cout << '\t' << std::endl;
	

	long_vector.push_back(25);
	long_vector.push_back(0);

	
	// также у std::vector присутствует весь функционал std::array
	// размер, сортировка, итераторы, копирование, обмен и т.д.

	std::sort(long_vector.begin(), long_vector.end());

	std::cout << "vector<> after sort() ";
	for (long long value : long_vector)
		std::cout << '\t' << value;
	

	long_vector[2] = 11;


	// вставка в vector
	long_vector.insert(long_vector.begin() + 2, 100500/* с помощью шаблона можно вставить несколько*/);
	std::cout << "vector<> after insert() ";
	for (long long value : long_vector)
		std::cout << '\t' << value;
	

	// std::list
	// Всё наоборот: вставка и увеличение списка производится быстрее, чем в vector,
	// а доступ - медленнее, причём чем дальше от 1-го элемента - тем МЕДЛЕННЕЕ

	// Так как для доступа к i-тому элементу (определения его адреса в ОП) необходимо
	// пройти по цепочке от 1-го элемента


	// В отличие от array и vector не является копмпактным массивом в памяти,
	// элементы std::list представлены отдельными объектами, связанными друг с другом
	// указателями, которые они хранят
	// [null|данные|указатель] ---> [указатель|данные|указатель] 
	//													/
	//											     /
	//                                            /
	//                                         /
	//                                      /
	//							 [указатель|данные|указатель] <---> [указатель|данные|null]


	std::list<char> char_list = { 'b' };
	char_list.push_front('a'); // вставка перед новым элементом
	char_list.push_back('b'); // вставка после последнего элемента

	for (char ch : char_list)
	{
		std::cout << ch << std::endl;
	}

	// Если класс простой и не содержит динамических данных
	class  sample_class
	{
		 int a;
		 double b;
	};
	// список можно составлять из объектов класса
	std::list<sample_class> obj_list1;
	// в противном случае - из указателей на объекты
	std::list<sample_class*> obj_list2;


	// std::map
	// Состоит из пар "ключ" - "значение"
	// Например "Tu-154" - 5000, "B777" - 6000, "A320" - 6500
	std::map<std::string, double> plane_and_range = 
	{ {"Tu-154" , 5000}, {"B777" , 6000}, {"A320" , 6500 } };
	plane_and_range.insert(std::pair<std::string, double>("A380", 6500 ));

	std::cout << "The range of Boening 777 is " << plane_and_range["B777"] << std::endl;
	std::cout << "The range of A380 is " << plane_and_range["A380"] << std::endl;
	


	double d_array[5] = { 1, 2, 3, 4, 5 };
	int i = 600000;
	try //в try помещается код, потенциально способный вызвать исключения:
		// функции, не получающие доступа к фаулам, портам, устройствам и т.д.
		// функции, неправильно работающие с памятью
	{
		d_array[i] = 15;
	}
	catch (std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Error in \" d_array[i] = 15\"" << std::endl;
	}

	


	getchar();
	return 0;
}

//  В курс не вошли темы, необходимые для штатной коммерческой разработки ПО:

// * юнит-тестирование (на каждый модуль или проект создаётся проект, который
// производит тестирование, сборка и запуск sln настраивается таким образом, что
// в первую очередь собирается базовый проект, затем собирается код теста и запускается)
// * технологии, процессы и шаблоны разработки (SCRUM, Waterfall, Agile, ...)
// * создание графических приложений
// * многопоточные и асинхронные приложения
// 